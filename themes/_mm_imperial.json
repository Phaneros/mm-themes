{
    "$schema": "../palette.schema.json",
    "name": "MM Imperial",
    "palette": {
        "textColour": "#ebb",
        "terminalColour": "#dd3311",
        "focusHighlight": "#ee6688",
        "highlightText1": "#f77",
        "highlightWarning": "#ff7700",  // warnings & debug buttons
        "highlightError": "#ff0066",
        "buttonHighlight": "#cc33ff",
        "sidebarHighlight": "#ff8877",

        "highlightModified": "#1188cc",
        "highlightModifiedText": "#11aaee",
        "highlightAdded": "#44bb44",
        "highlightAddedText": "#66ee55",

        "backgroundDarkest": "#000000",
        "background1": "#080000",
        "background2": "#0f0000",
        "background3": "#220606",
        "background4": "#440a0a",
        "background5Selection": "#551020",
        "background6": "#661122",
        "background7": "#881133",

        "terminalSelection": "#523",
        
        "borderColour": "#553300",
        "borderBright": "#774400",
        "contrastLowlight": "#886633",
        
        "parallelDarkest": "#220000",
        "parallelDark": "#441100",
        "parallelUpperMid": "#802a2a",
        "parallelBright": "#990044",  // minimap slider

        "adjacentMid": "#ca7249",
        "adjacentLowerMid": "#a14933",
    },
    "colors": {
        // general
        "focusBorder": "${focusHighlight}",
        "panel.border": "${borderColour}",
        "foreground": "${textColour}",
        "descriptionForeground": "${textColour}",
        "icon.foreground": "${focusHighlight}",
        "scrollbarSlider.background": "${parallelUpperMid}99",
        "scrollbarSlider.hoverBackground": "${parallelBright}99",
        "scrollbarSlider.activeBackground": "${focusHighlight}99",
        "editor.selectionBackground": "${background5Selection}ee",
        "selection.background": "${background5Selection}",
        "textLink.foreground": "${focusHighlight}",
        "textLink.activeForeground": "${buttonHighlight}",

        // settings
        "button.background": "${background6}",
        "button.secondaryBackground": "${background6}",
        "settings.checkboxBackground": "${parallelUpperMid}",
        "settings.checkboxForeground": "${textColour}",
        "settings.dropdownBackground": "${background3}",
        "settings.dropdownBorder": "${focusHighlight}",
        "settings.focusedRowBackground": "${background4}",
        "keybindingLabel.background": "${background6}",
        "keybindingLabel.border": "${background1}",

        // header
        "titleBar.activeBackground": "${background4}",
        "titleBar.inactiveBackground": "${background2}",
        "titleBar.border": "${borderColour}",

        // sidebar selector
        "activityBar.background": "${backgroundDarkest}",
        "activityBar.border": "${borderColour}",
        "activityBar.foreground": "${sidebarHighlight}",
        "activityBar.inactiveForeground": "${parallelUpperMid}",

        // sidebar
        "sideBar.background": "${background1}",
        "sideBar.border": "${borderColour}",
        "sideBarSectionHeader.background": "${background3}",
        "sideBarSectionHeader.border": "${borderColour}",
        "badge.background": "${background6}",
        
        // file system view
        "list.activeSelectionBackground": "${background6}",
        "list.inactiveSelectionBackground": "${background6}",
        "list.hoverBackground": "${background5Selection}",
        "list.dropBackground": "${parallelDark}",
        
        // git
        "gitDecoration.modifiedResourceForeground": "${highlightModifiedText}",
        "gitDecoration.addedResourceForeground": "${highlightAddedText}",
        
        // terminal
        "terminal.background": "${backgroundDarkest}",
        "terminalCursor.foreground": "${terminalColour}",
        "terminal.foreground": "${textColour}",
        "terminal.selectionBackground": "${terminalSelection}",
        "terminal.ansiBrightYellow": "${terminalColour}",  // PowerShell defaults to this colour

        // debugging
        "debugToolBar.background": "${background3}",
        "debugIcon.continueForeground": "${buttonHighlight}",
        "debugIcon.pauseForeground": "${buttonHighlight}",
        "debugIcon.stepOverForeground": "${focusHighlight}",
        "debugIcon.stepIntoForeground": "${focusHighlight}",
        "debugIcon.stepOutForeground": "${focusHighlight}",
        "debugIcon.restartForeground": "${highlightWarning}",
        "debugIcon.stopForeground": "${highlightError}",
        "debugIcon.breakpointCurrentStackframeForeground": "${highlightWarning}",
        "debugView.stateLabelBackground": "${background4}",
        
        // bottom-most bar
        "statusBar.background": "${background3}",
        "statusBar.border": "${borderColour}",
        "statusBar.debuggingBackground": "${background6}",
        "statusBar.foreground": "${textColour}",

        // open file tabs
        "tab.border": "${borderColour}",
        "tab.activeBorder": "${focusHighlight}",
        "tab.activeForeground": "${textColour}",
        "tab.activeBackground": "${background5Selection}",
        "tab.hoverForeground": "${textColour}",
        "tab.hoverBackground": "${parallelDarkest}",
        "tab.inactiveForeground": "#b88",
        "list.warningForeground": "${highlightWarning}dd",
        "tab.inactiveBackground": "${background2}",
        "editorGroupHeader.tabsBackground": "${backgroundDarkest}",
        "editorGroupHeader.tabsBorder": "${borderColour}",

        // editor pane
        "editor.background": "${background1}",
        "editor.foreground": "${textColour}",
        "editor.lineHighlightBorder": "${background6}88",
        "editor.selectionHighlightBackground": "${parallelDark}88",
        "editor.wordHighlightBackground": "${parallelDark}99",
        "editor.wordHighlightStrongBackground": "${parallelDark}aa",
        "editor.hoverHighlightBackground": "${background5Selection}99",

        "editorGutter.background": "${background2}",
        "editorGutter.modifiedBackground": "${highlightModified}",
        "editorGutter.addedBackground": "${highlightAdded}",
        "editorLineNumber.activeForeground": "${buttonHighlight}",
        "editorLineNumber.foreground": "${contrastLowlight}",
        "editorIndentGuide.background": "${borderColour}",
        "editorIndentGuide.activeBackground": "${borderBright}",

        "editorSuggestWidget.background": "${background3}",
        "editorSuggestWidget.highlightForeground": "${focusHighlight}",
        "editorSuggestWidget.focusHighlightForeground": "${highlightText1}",

        "editor.findMatchBackground": "${background5Selection}",  // current match
        "editor.findMatchBorder": "${background6}",
        "editor.findMatchHighlightBackground": "${background7}99",  // other matches
        "editor.findMatchHighlightBorder": "${background5Selection}",  // other matches
        
        // peek view (e.g. find usages)
        "peekViewEditor.matchHighlightBackground": "#00995555",
        "peekViewEditor.matchHighlightBorder": "#55990088",
        "peekViewEditorGutter.background": "#220011",
        "peekViewEditor.background": "#001111",

        // minimap
        "minimap.background": "${background1}00",
        "minimap.findMatchHighlight": "${adjacentLowerMid}77",
        "minimap.warningHighlight": "${highlightWarning}77",
        "minimap.errorHighlight": "${highlightError}77",
        "minimap.selectionHighlight": "${background7}88",
        "minimap.selectionOccurrenceHighlight": "${background7}66",

        "minimapGutter.modifiedBackground": "${highlightModified}",
        "minimapGutter.addedBackground": "${highlightAdded}",
        "minimapSlider.background": "${parallelBright}33",
        "minimapSlider.hoverBackground": "${parallelBright}33",
        "minimapSlider.activeBackground": "${parallelBright}44",

        "editorOverviewRuler.background": "#002211",

        // hover widget
        "editorHoverWidget.border": "${borderColour}",
        "editorHoverWidget.background": "${background3}",
        
        // dropdowns
        "menu.background": "${background3}",
        "menu.selectionBackground": "${background6}",
        "menu.foreground": "${textColour}",
        
        // input
        "quickInput.background": "${background3}",
        "quickInputList.focusBackground": "${background6}",
        "input.background": "${background1}",
        "input.border": "${borderColour}",
        "editorWidget.background": "${background3}",
        "inputValidation.errorBackground": "${background6}",
        "inputValidation.errorForeground": "${highlightText1}",
        
        // brackets
        "editorBracketHighlight.foreground1": "#2cd",
        "editorBracketHighlight.foreground2": "#9af",
        "editorBracketHighlight.foreground3": "#8f8",
        "editorBracketHighlight.foreground4": "#fbe",
        "editorBracketHighlight.unexpectedBracket.foreground": "#f33",
    },
    "semanticHighlighting": true,
}