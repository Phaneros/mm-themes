
from json.decoder import JSONDecodeError
import os, json, re, sys

macro_pattern = re.compile(r'\$\{(\w+)\}')
replace_commas_pattern = re.compile(r',([\s]*)([\}\]])', re.MULTILINE)


def load_jsonc(filename: str) -> dict:
    with open(filename, 'r') as f:
        lines = f.readlines()
    # remove comments, but only outside strings
    for line_index in range(len(lines)):
        line_chunks = lines[line_index].split('"')
        for chunk_index in range(0, len(line_chunks), 2):
            if '//' in line_chunks[chunk_index]:
                line_chunks = line_chunks[:chunk_index+1]
                line_chunks[-1] = line_chunks[-1].split('//', 1)[0]
                lines[line_index] = '"'.join(line_chunks) + '\n'
                break
    # remove invalid commas (why isn't this built in to JSON parsers?)
    str_contents = ''.join(lines)
    str_contents_multiline = re.sub(replace_commas_pattern, r'\1\2', str_contents)
    try:
        contents: dict = json.loads(str_contents_multiline)
    except JSONDecodeError as ex:
        lines = str_contents_multiline.split('\n')
        line_info_msg = f'\nfile: {filename}\n\'{lines[ex.lineno-1]}\''
        ex.msg += line_info_msg
        ex.args = tuple(x + line_info_msg for x in ex.args)
        raise ex
    return contents


themes_dir = 'themes'
token_colours_filename = os.path.join(themes_dir, 'syntax_highlighting.json')
for filename in os.listdir(themes_dir):
    if filename[0] != '_':
        continue
    print(f'Applying themes to {filename}')
    contents = load_jsonc(os.path.join(themes_dir, filename))
    token_colours = load_jsonc(token_colours_filename)
    palette = contents.pop('palette')
    colours = contents['colors']
    for key in colours.keys():
        match = re.match(macro_pattern, colours[key])
        if match:
            palette_colour = match.group(1)
            if palette_colour not in palette.keys():
                print(f'ERROR: key {palette_colour} not in palette!')
                sys.exit(1)
            colours[key] = re.sub(macro_pattern, palette[palette_colour], colours[key])
    contents['tokenColors'] = token_colours['tokenColors']
    contents['semanticTokenColors'] = token_colours['semanticTokenColors']

    contents['$schema'] = 'vscode://schemas/color-theme'
    target_file_name = filename[1:]
    with open(f'{themes_dir}/{target_file_name}', 'w') as f:
        print('///// ', file=f)
        print('///// GENERATED FILE -- DO NOT EDIT', file=f)
        print('///// ', file=f)
        json.dump(contents, fp=f, indent=4)


